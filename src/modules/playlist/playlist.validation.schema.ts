import * as yup from 'yup';

export const createOrPutSchemaBody = yup.object().shape({
  name: yup.string().required().min(2).max(20),
  song_ids: yup.array().required(),
  genres: yup.array()
});

export const createOrPutSchemaParams = yup.object().shape({
  user_id: yup.string().required()
});

export const patchSchemaBody = yup.object().shape({
  name: yup.string().min(2).max(20),
  song_ids: yup.array(),
  genres: yup.array()
}).test('at-least-one-field', "you must provide at least one field", value =>
!!(value.name || value.songs || value.genres));

export const addOrRemoveSongSchemaBody = yup.object().shape({
  song_id: yup.string().required(),
});