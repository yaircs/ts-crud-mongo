
  import raw from "../../middlewares/route.async.wrapper.js";
  import * as playlist_service from "./playlist.services.js";
  import express, { Request, Response } from 'express';
  import { playlistValidateCreateOrPutBody, playlistValidateCreateOrPutParams, playlistValidatePatchBody, playlistValidateSongsAddOrRemoveParams } from '../../middlewares/playlist.validations.js';
  
  
const playlistRouter = express.Router();
playlistRouter.use(express.json())

playlistRouter.route('/')
    .get(raw( async (req: Request, res: Response): Promise<void> => {
        const playlists = await playlist_service.get_all_playlists();
        res.status(200).json(playlists);
    }))

playlistRouter.route('/:user_id')
    .post(playlistValidateCreateOrPutBody, playlistValidateCreateOrPutParams, raw( async (req: Request, res: Response): Promise<void> => {
        let playlist = await playlist_service.create_playlist({...req.params, ...req.body});
        res.status(200).json(playlist);
    }));

playlistRouter.route('/:id')
    .get(raw( async (req: Request, res: Response): Promise<void> => {
        const playlist = await playlist_service.get_playlist_by_id(req.params.id);
        if (!playlist) res.status(404).json({ status: 400, message: `Playlist ${req.params.id} not found.` });
        else res.status(200).json(playlist);
    }))
    .put(playlistValidateCreateOrPutBody, raw( async (req: Request, res: Response): Promise<void> => {
        let updatedPlaylist = await playlist_service.find_playlist_by_id_and_update(req.params.id, req.body);
        if (!updatedPlaylist) res.status(404).json({ status: 400, message: `Playlist ${req.params.id} not found.` });
        else res.status(200).json(updatedPlaylist);
    }))
    .patch(playlistValidatePatchBody ,raw( async (req: Request, res: Response): Promise<void> => {
        let updatedPlaylist = await playlist_service.find_playlist_by_id_and_update(req.params.id, req.body);
        if (!updatedPlaylist) res.status(404).json({ status: 400, message: `Playlist ${req.params.id} not found.` });
        else res.status(200).json(updatedPlaylist);
    }))
    .delete(raw( async (req: Request, res: Response): Promise<void> => {
        let deletedPlaylist = await playlist_service.delete_playlist_by_id(req.params.id);
        if (!deletedPlaylist) res.status(404).json({ status:400, message: `Playlist ${req.params.id} not found.` });
        else res.status(200).json(deletedPlaylist);
    }));

    playlistRouter.route('/:id/add-song/:song_id')
     .patch(playlistValidateSongsAddOrRemoveParams, raw( async (req: Request, res: Response): Promise<void> => {
        let playlist = await playlist_service.find_playlist_by_id_and_add_song(req.params.id ,req.params.song_id);
        res.status(200).json(playlist);
    }));
 
    playlistRouter.route('/:id/remove-song/:song_id')
        .patch(playlistValidateSongsAddOrRemoveParams, raw( async (req: Request, res: Response): Promise<void> => {
        let playlist = await playlist_service.find_playlist_by_id_and_remove_song(req.params.id ,req.params.song_id);
        res.status(200).json(playlist);
    }));
 
  
  export default playlistRouter;
  