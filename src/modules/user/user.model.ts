import mongoose from 'mongoose';
const { Schema, model } = mongoose;

const UserSchema = new Schema({
    username    : String,
    email       : String,
    password    : String,
    playlist_ids   : [{ type: Schema.Types.ObjectId, ref:'playlist'}]
}, {timestamps: true});
  
export default model('user', UserSchema);