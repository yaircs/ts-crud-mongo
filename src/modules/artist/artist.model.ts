import mongoose from 'mongoose';
const { Schema, model } = mongoose;

const ArtistSchema = new Schema({
    name  : { type : String, required : true },
    song_ids : [{ type: Schema.Types.ObjectId, ref:'song'}],
}, {timestamps: true});
  
export default model('artist', ArtistSchema);
