import { ArtistData } from '../../typings/interfaces.js';
import adapter from './artist.mongo.adapter.js';
import * as song_service from '../song/song.services.js';

// GET /api/arists/
export const get_all_artists = async (): Promise<ArtistData[] | null> => {
    const artists = await adapter.get_all_artists();
    return artists;
}

// POST /api/arists/
export const create_artist = async (payload: ArtistData): Promise<ArtistData> => {
    const newArtist = await adapter.create_artist(payload);
    return newArtist;
}

// GET /api/arists/:id
export const get_artist_by_id = async (artist_id: string): Promise<ArtistData | null> => {
    const artist = await adapter.find_artist_by_id(artist_id);
    return artist;
}

// PUT /api/artists/:id
export const find_artist_by_id_and_update = async (artist_id: string, payload: ArtistData): Promise<ArtistData | null> => {
    const updatedArtist = await adapter.find_artist_by_id_and_update(artist_id, payload);
    return updatedArtist;
}

// delete /api/artists/:id
export const delete_artist_by_id = async (artist_id: string): Promise<ArtistData | null> => {
    const deletedArtist = await adapter.find_artist_by_id_and_delete(artist_id);
    // Also delete all artist's songs and remove them from all containing playlists
    deletedArtist?.song_ids.forEach(async song_id => await song_service.find_song_by_id_and_delete(song_id));
    return deletedArtist;
}


// patch /api/artists/:id/add-song/:song_id
export const find_artist_by_id_and_add_song = async (artists_id: string, song_id: string): Promise<ArtistData | null> => {
    const AddedSongArtist = await adapter.find_artist_by_id_and_add_song(artists_id, song_id);
    return AddedSongArtist;
}

// Service in use in DELETE /api/songs/:id
export const find_artist_by_id_and_remove_song = async (artist_id: string, song_id: string): Promise<ArtistData | null> => {
    const RemovedSongArtist = await adapter.find_artist_by_id_and_remove_song(artist_id, song_id);
    return RemovedSongArtist;
}