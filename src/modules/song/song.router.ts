
  import raw from "../../middlewares/route.async.wrapper.js";
  import * as song_service from "./song.services.js";
  import express, { Request, Response } from 'express';
  import { songValidateCreateOrPutBody, songValidateCreateOrPutParams, songValidatePatch } from '../../middlewares/song.validations.js';
  
  
const songRouter = express.Router();
songRouter.use(express.json())

songRouter.route('/')
    .get(raw( async (req: Request, res: Response): Promise<void> => {
        const songs = await song_service.get_all_songs();
        res.status(200).json(songs);
    }))

songRouter.route('/:artist_id')
    .post(songValidateCreateOrPutBody, songValidateCreateOrPutParams, raw( async (req: Request, res: Response): Promise<void> => {
        let song = await song_service.create_song({...req.params, ...req.body});
        res.status(200).json(song);
    }));


songRouter.route('/:id')
    .get(raw( async (req: Request, res: Response): Promise<void> => {
        const song = await song_service.get_song_by_id(req.params.id);
        if (!song) res.status(404).json({ status: 400, message: `Song ${req.params.id} not found.` });
        else res.status(200).json(song);
    }))
    .put(songValidateCreateOrPutBody, raw( async (req: Request, res: Response): Promise<void> => {
        let updatedSong = await song_service.find_song_by_id_and_update(req.params.id, req.body);
        if (!updatedSong) res.status(404).json({ status: 400, message: `Song ${req.params.id} not found.` });
        else res.status(200).json(updatedSong);
    }))
    .patch(songValidatePatch ,raw( async (req: Request, res: Response): Promise<void> => {
        let updatedSong = await song_service.find_song_by_id_and_update(req.params.id, req.body);
        if (!updatedSong) res.status(404).json({ status: 400, message: `Song ${req.params.id} not found.` });
        else res.status(200).json(updatedSong);
    }))
    .delete(raw( async (req: Request, res: Response): Promise<void> => {
        let deletedSong = await song_service.find_song_by_id_and_delete(req.params.id);
        if (!deletedSong) res.status(404).json({ status:400, message: `Song ${req.params.id} not found.` });
        else res.status(200).json(deletedSong);
    }));
 
 
  
  export default songRouter;
  