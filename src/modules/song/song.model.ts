import mongoose from 'mongoose';
const { Schema, model } = mongoose;

const SongSchema = new Schema({
    name     : { type : String, required : true },
    artist_id   : { type: Schema.Types.ObjectId, ref:'artist'},
    duration : { type : String, required : true},
    genre    : { type : String }
}, {timestamps: true});
  
export default model('song', SongSchema);
