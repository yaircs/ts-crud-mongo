import log from '@ajar/marker';
import app from './modules/app/app.js';
import { connect_db } from './db/mongoose.connection.js';

const { PORT = 8080,HOST = 'localhost', DB_URI} = process.env;

;(async ()=> {
  await connect_db(DB_URI as string);  
  await app.listen(PORT as number, HOST as string);
  log.magenta(`api is live on`,` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`);  
})().catch(console.log)