import { Request, Response, NextFunction } from 'express';
import { createOrPutSchema, patchSchema } from '../modules/user/user.validation.schema.js';

export function userValidateCreateOrPut(req: Request, res: Response, next: NextFunction) {
    createOrPutSchema.validate(req.body).then((valid) => {
        next();
    }).catch(next);
}

export function userValidatePatch(req: Request, res: Response, next: NextFunction) {
    patchSchema.validate(req.body).then((valid) => {
        next();
    }).catch(next);
}