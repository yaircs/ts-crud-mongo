import fs from 'fs-extra';
import log from '@ajar/marker';
import { Request, Response, NextFunction } from 'express';

const { White, Reset, Red } = log.constants;
const { NODE_ENV } = process.env;
import HttpException from '../modules/exceptions/Http.exception.js';
import UrlNotFoundException from '../modules/exceptions/UrlNotFound.exception.js';
import  { ErrorResponse }  from '../typings/interfaces.js';

export const printErrorMiddleware = (err: HttpException, req: Request, res: Response, next: NextFunction): void => {
    log.error(err);
    next(err)
}

export const errorResponseMiddleware =  (err: HttpException, req: Request, res: Response, next: NextFunction): void => {
    const response: ErrorResponse = { 
        status: err.status || 500, 
        message: err.message || 'internal server error'
      };

      if(NODE_ENV !== 'production') response.stack = response.stack;
      res.status(response.status).json(response);
}

export const notFoundMiddleware = (req: Request, res: Response, next: NextFunction): void => {
    next(new UrlNotFoundException(req.url));
}


export function logErrorMiddleware(path:string){
    const errorsFileLogger = fs.createWriteStream(path, { flags: 'a' });
    
    return (error: HttpException, req: Request, res: Response, next: NextFunction) => {
        errorsFileLogger.write(`${error.status} :: ${error.message} >> ${error.stack} \n`);
        next(error);
    }
  }

