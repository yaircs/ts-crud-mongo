
import fs from 'fs';
import { NextFunction, Request, Response } from 'express';


export function logHttpMiddleware(path:string){
    const httpFileLogger = fs.createWriteStream(path, { flags: 'a+' });
    
    return (req: Request, res: Response, next: NextFunction) => {
        httpFileLogger.write(`${req.method} :: ${req.originalUrl} >> ${ Date.now() } \n`);
        next()
    }
}